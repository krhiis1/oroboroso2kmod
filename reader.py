import hashlib
import csv
from iocbio.kinetics.handler.experiment_generic import ExperimentGeneric
from iocbio.kinetics.io.data import Data, Carrier
from .experiment import ExperimentO2k
from iocbio.kinetics.constants import database_table_experiment
import os.path
import re
import array as arr
import numpy
import collections

IocbioKineticsModule = ["reader","args"]

def args(parser):
    parser.add(name='o2k_chamber', help='The selection of the Chamber of Interest. For example: 4A, 4B')
    parser.add(name="date_time", help = "The date of the experiment in the format of YYYY-MM-DD")
    return '''Oroboros 2K Protocols:
------------------
O2K - Generic O2K Protocol
'''

def reader( txt , o2kchamber):
    text = csv.DictReader(txt.splitlines())

    e=collections.defaultdict(list)

    for row in text:
        for k in row.keys():
            try:
                e[k].append(float(row[k]))
            except:
                if k == "Event Name" or k == "Chamber" or k == "Event Text":
                        e[k].append(row[k])
                elif row[k] == '' or row[k] == '""':
                        e[k].append(0)


    events = {}

    time = e["Time [min]"]

    eventname = e["Event Name"]
    chamber = e["Chamber"]


    for i in range(len(chamber)):
        if o2kchamber == "4A":
            if chamber[i] == "Left" or chamber[i] == "Both":
                events[time[i]] = eventname[i]
        elif o2kchamber == "4B" :
            if chamber[i] == "Right" or chamber[i] == "Both":
                events[time[i]] = eventname[i]


    e['events'] = events

    print(time)
    return e, numpy.array(time)

def create_data(database, experiment_id = None, args= None):
    filename=getattr(args, "file_name", None)
    o2kchamber = getattr(args, "o2k_chamber", None)
    date = getattr(args, "date_time", None)
    fulltxt = None





    if experiment_id is not None:
        fulltxt, name , o2kchamber, date= ExperimentO2k.get_fulltxt(database, experiment_id)


    if fulltxt is None and filename is not None and o2kchamber is not None:
        if filename.find("csv") != -1:
            head, cname = os.path.split(filename)
            name = cname + "-" + o2kchamber
            fulltxt = open(filename, encoding = "ISO-8859-1").read()

    if fulltxt is None: return None

    d, x = reader(fulltxt, o2kchamber)


    if experiment_id is None:
        expid = "Oroboros 2K - " + o2kchamber + hashlib.sha256(fulltxt.encode("utf-8")).hexdigest()
    else:
        expid = experiment_id


    dd = {"o2":{"x" : x,
    "y": Carrier("O2 concentration", "um", numpy.array(d[o2kchamber + ": O2 concentration [µM]"]))},
    "vo2": { "x" : x,
    "y": Carrier("O2 flow per cells","pmol/(s*Mill)", numpy.array(d[o2kchamber + ": O2 flow per cells [pmol/(s*Mill)]"]))},
    "Amp":{"x" : x,
    "y": Carrier ("Amp", "um", numpy.array(d[o2kchamber + ": Amp [µM]"]))},
    "Amp slope":{"x" : x,
    "y": Carrier ("Amp slope", "pmol/(s*Mill)", numpy.array(d[o2kchamber + ": Amp slope [pmol/(s*mL)]"]))},
    "Block temp":{"x" : x,
    "y": Carrier("Block temp", "C", numpy.array(d["4: Block temp. [°C]"]))},
    "Peltier power": {"x": x,
    "y": Carrier("Peltier power", "%", numpy.array(d["4: Peltier power [%]"]))},
    "pX": {"x": x,
    "y": Carrier("Potentiometric raw signal", "V", numpy.array(d[o2kchamber + ": pX [1]"]))},
    "pX slope": {"x": x,
    "y": Carrier("Potentometric raw signal slope", "dV/dt", numpy.array(d[o2kchamber + ":pX slope [pX*10^-3/s]"]))},
    "Room temp":{ "x" : x,
    "y": Carrier("Room temperature", "C", numpy.array(d["4: Room temp. [°C]"]))},
    "Temp. ext. channel": {"x": x,
    "y": Carrier("Temperature through external channel", "C", numpy.array(d["4: Temp. ext. channel [°C]"]))}}


    data = Data( expid,
                 config = d,
                 type = "Oroboros 2K experiment",
                 type_generic = "2K experiment",
                 time = date,
                 name = name,
                 xname = "Time",
                 xunit = "min",
                 xlim = (x[0],x[-1]),
                 data = dd)


    if not ExperimentGeneric.has_record(database, data.experiment_id):
        database.set_read_only(False)
        ExperimentO2k.store(database, data, fulltxt, name, filename, date, o2kchamber)

    return data
