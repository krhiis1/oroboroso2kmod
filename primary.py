from iocbio.kinetics.calc.linreg import AnalyzerLinRegress
from iocbio.kinetics.calc.generic import Stats, XYData, AnalyzerGeneric
from iocbio.kinetics.calc.composer import AnalyzerCompose
from iocbio.kinetics.constants import database_table_roi, database_table_experiment
from iocbio.kinetics.calc.bump import AnalyzerBumpDatabase

from PyQt5.QtCore import pyqtSignal, QObject


IocbioKineticsModule = ['analyzer', "database_schema"]
class AnalyzerRespirationSignals(QObject):
    sigUpdate = pyqtSignal()

class AnalyzerRespiration(AnalyzerLinRegress):

    database_table = "O2kchamber_o2"
    database_conc_view = "O2kchamber_o2_conc"

    @staticmethod
    def database_schema(db):
        db.query("CREATE TABLE IF NOT EXISTS " + db.table(AnalyzerRespiration.database_table) +
                 "(data_id text PRIMARY KEY, " +
                 "rate double precision, " +
                 "FOREIGN KEY (data_id) REFERENCES " + db.table(database_table_roi) + "(data_id) ON DELETE CASCADE" +
                 ")")
        if not db.has_view(AnalyzerRespiration.database_conc_view):
            db.query("CREATE VIEW " + db.table(AnalyzerRespiration.database_conc_view) + " AS SELECT "
                    "r.experiment_id, rate, event_value, event_name FROM " + db.table("roi") + " r join " +
                    db.table(AnalyzerRespiration.database_table) + " v on r.data_id = v.data_id")

    def __init__(self, database, data):
        self.database_schema(database)
        AnalyzerLinRegress.__init__(self, data.x("o2"), data.y("o2").data)

        self.signals = AnalyzerRespirationSignals()
        self.database = database
        self.data = data
        self.data_id = data.data_id
        self.axisnames = XYData("Time", "O2")
        self.axisunits = XYData("min", "umol/l")

        self.fit()


    def fit(self):
        AnalyzerLinRegress.fit(self)
        c = self.database
        if self.database.has_record(self.database_table, data_id=self.data_id):
            c.query("UPDATE " + self.database.table(self.database_table) +
                    " SET rate=:rate WHERE data_id=:data_id",
                        rate =-self.slope,
                        data_id=self.data_id)
        else:
            c.query("INSERT INTO " + self.database.table(self.database_table) +
                    "(data_id, rate) VALUES(:data_id,:rate)",
                    rate =-self.slope,
                    data_id=self.data_id)
        self.stats["O2kchamber_o2"] = Stats("Respiration rate", "umol/l/min", -self.slope)
        self.signals.sigUpdate.emit()


    def update_reference_time(self, data):
        x0, x1 = data.xlim()
        events = data.config['events']
        # default reference
        self.t_reference = 0
        for et, ev in events.items():
            if et > x0 and et < x1:
                # the first reference within the region of interest
                self.t_reference = et
                break


    def remove(self):
        c = self.database
        c.query("DELETE FROM " + self.database.table(self.database_table) +
                " WHERE data_id=:data_id",
                data_id=self.data.data_id)
        self.database = None
        self.signals.sigUpdate.emit()

    def update_data(self, data):
        AnalyzerLinRegress.update_data(self, data.x("o2"), data.y("o2").data)
        self.fit()

    @staticmethod
    def eventvalue(event_name):
        try:
            # events are in form "beat Number"
            evalue = float(event_name.split()[1])
        except:
            evalue = event_name
        return evalue

    def update_event(self, event_name):
        self.data.event_name = event_name
        self.data.event_value = 0

    @staticmethod
    def slice(data, x0, x1):
        print(x0, x1)
        # create a slice of data
        sdata = data.slice(x0, x1)
        # determine event corresponding to the data if any
        events = data.config['events']
        # default, if we don't find any
        sdata.event_name = ""
        sdata.event_value = None
        for et, ev in events.items():
            if et > x0 and et < x1:
                # the first reference within the region of interest
                sdata.event_name = ev
                sdata.event_value = AnalyzerRespiration.eventvalue(ev)
                break
        return sdata




# Module API
def analyzer(database, data):
    Analyzer = {}
    if data.type_generic == "2K experiment":
        # as a key, we define type of ROI used later in secondary analyzers as well
        Analyzer['default'] = AnalyzerRespiration
    # only primary analyzer is returned this time
    return Analyzer, None, None

def database_schema(db):
    AnalyzerRespiration.database_schema(db)