import os
import csv
import argparse
from iocbio.kinetics.handler.experiment_generic import ExperimentGeneric
from iocbio.kinetics.constants import database_table_experiment
import os.path

IocbioKineticsModule = ["database_schema"]

class ExperimentO2k(ExperimentGeneric):
    
    database_table = "o2kchamber"


    @staticmethod
    def database_schema(db):
        db.query("CREATE TABLE IF NOT EXISTS " + db.table(ExperimentO2k.database_table) +
                 "(experiment_id text PRIMARY KEY,"
                 " rawdata text,"
                 " filename text,"
                 " title text,"
                 " o2kchamber text,"
                 "name text,"
                 "date text," +
                 "FOREIGN KEY (experiment_id) REFERENCES " + db.table(database_table_experiment) + "(experiment_id) ON DELETE CASCADE" +
                 ")")

    def get_fulltxt(database, experiment_id):
        experiment_id = experiment_id
        if ExperimentGeneric.hardware(database, experiment_id) != "Oroboros 2K":
            return none
        print("Loading raw data for experiment:", experiment_id)
        for q in database.query("SELECT rawdata, name, o2kchamber, date FROM " + database.table(ExperimentO2k.database_table) +
                                " WHERE experiment_id=:experiment_id",
                                experiment_id=experiment_id):
            return q.rawdata, q.name , q.o2k_chamber, q.date
        return None, None, None, None

    def store(database, data, fulltxt, name, filename, date, o2kchamber):
        experiment_id = data.experiment_id

        ExperimentGeneric.store(database,experiment_id, time=data.time,
                                type_generic = data.type_generic,
                                type_specific = data.type, hardware= "Oroboros 2K")

        c = database

        if not database.has_record(ExperimentO2k.database_table, experiment_id = experiment_id):
            c.query("INSERT INTO " + database.table(ExperimentO2k.database_table) +
                   "(experiment_id, rawdata, filename, name, title, date, o2kchamber) " +
                   "VALUES(:experiment_id,:ftxt,:finame, :fname,:exptitle,:exptime,:expchamber)",
                   experiment_id=experiment_id,
                   ftxt=fulltxt,
                   finame= filename,
                   fname= name,
                   exptitle="Oroboros 2K experiment", # had some bug so had to remove
                   exptime = date,
                   expchamber = o2kchamber )


# definition of database_schema function
def database_schema(db):
    # just in case if our experiment is the first one, make general if needed
    ExperimentGeneric.database_schema(db)
    # create table for our experiment
    ExperimentO2k.database_schema(db)